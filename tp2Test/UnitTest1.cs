using System;
using Xunit;
using tp2Console;

namespace tp2Test
{
    public class UnitTest1
    {
        [Fact]
        public void TestLongueur()
        {
            Assert.Equal(0, Program.Longueur(""));
            Assert.Equal(5, Program.Longueur("Salut"));
            Assert.Equal(6, Program.Longueur("Salut.        ")); /*Les espaces ne doivent pas être pris en compte pour que le programme puisse détecter le point à la fin.*/
            Assert.Equal(5, Program.Longueur("      Salut"))
        }

        [Fact]
        public void TestMajuscule()
        {
            /* Ici, true confirme que la phrase commence par une majuscule. */
            Assert.Equal(false, Program.Majuscule(""));
            Assert.Equal(false, Program.Majuscule("salut"));
            Assert.Equal(true, Program.Longueur("Salut"));
            Assert.Equal(true, Program.Longueur("     Salut")); /* Comme au dessus : les espaces ne doivent pas être pris en compte */
        }

        public void TestPoint()
        {
            /* Ici, true confirme que la phrase termine par un point. */
            Assert.Equal(false, Program.Majuscule(""));
            Assert.Equal(false, Program.Majuscule("salut"));
            Assert.Equal(true, Program.Longueur("Salut."));
            Assert.Equal(true, Program.Longueur("Salut.       "));
        }
    }
}
